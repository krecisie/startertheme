<?php
/**
 * Template Name: Kariera
 */

get_header(); ?>

    <section class="section--full page-carrer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7 page-offer__content">
                    <div class="page-styles-default">
                        <h2 class="page-offer__header"><?php _e('Kariera', 'sag'); ?></h2>
                        <?php if(have_posts()): while(have_posts()): the_post(); ?>
                        <p class="page-offer__text">
                            <?php the_content(); ?>
                        </p>
                        <?php endwhile; endif; ?>

                        <?php $posts = get_posts(array('post_type' => 'kariera', 'posts_per_page' => -1, 'suppress_filters' => 0));
                        if ($posts): foreach ($posts as $post):
                            setup_postdata($post); ?>

                            <article class="career clearfix">
                                <div class="career__container">
                                    <h3 class="career__title"><?php the_title(); ?></h3>
<!--                                    <span class="career__date">Data dodania: </span>-->
<!--                                    <span class="career__date career__date--color">--><?php //the_date(); ?><!--</span>-->
                                    <p class="career__description"><?php the_content(); ?></p>
                                </div>
                                <div class="career__cta">
                                    <?php if (get_field('status')) {
                                        echo '<span class="btn btn--label button-text">' . __('Aktualne', 'sag' ) . '</span>
                                    <a href="#" class="popmake-539 popmake-1745 popmake-1746"><span class="btn button-text btn--color-black btn--transparent popmake-165">' . __('Aplikuj', 'sag' ) . '</span></a>';
                                    } else {
                                        echo '<span class="btn btn--label btn--disabled button-text">' . __('Nieaktualne', 'sag' ) . '</span>';
                                    }
                                    ?>
                                </div>
                            </article>


                        <?php endforeach;
                            wp_reset_postdata();
                        endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-md-push-1">
                    <?php
                    get_sidebar();
                    ?>
                </div>
            </div>
        </div>

    </section>

<?php wp_footer(); ?>
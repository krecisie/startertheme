<?php

?>

	<footer class="footer">
        <section class="section--footer">
            <div class="container">
                <div class="col-xs-4 col-md-3 col-md-offset-2 col-lg-3 col-lg-offset-2 footer__column">
                    <ul class="footer__list">
                        <li class="footer__item footer__item--bold"><?php _e( 'Menu', 'sag' ); ?></li>
                    </ul>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => 'ul', 'menu_class' => 'footer__list') ); ?>
                </div>
                <div class="col-xs-4 col-md-3 col-lg-3 footer__column">
                    <ul class="footer__list">
                        <li class="footer__item footer__item--bold"><?php _e( 'Siedziba', 'sag' ); ?></li>
                        <li class="footer__item"><?php _e( '40-432 Katowice', 'sag' ); ?></li>
                        <li class="footer__item"><?php _e( 'ul. Szopienicka 58a', 'sag' ); ?></li>
                        <li class="footer__item"><?php _e( 'Skr. Poczt. 3005', 'sag' ); ?></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-3 col-lg-3 footer__column">
                    <ul class="footer__list">
                        <li class="footer__item footer__item--bold"><?php _e( 'Pozostałe informacje', 'sag' ); ?></li>
                        <li class="footer__item"><?php _e( 'tel.:', 'sag' ); ?> +48 32 255 72 60</li>
                        <li class="footer__item"><?php _e( 'e-mail:', 'sag' ); ?> sag@sag.pl</li>
                        <li class="footer__item"><?php _e( 'KRS: 0000053066', 'sag' ); ?></li>
                    </ul>
                </div>
            </div>
        </section>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJiz3EN1z0Pl18PrAQaPJKHeknWGkmRiY">
</script>

</body>
</html>
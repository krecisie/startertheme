<?php
/**
Template Name: Kontakt
 */

get_header(); ?>

<section class="section--full page-contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7 page-contact__content">
                <h2 class="page-contact__header"><?php the_title(); ?></h2>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <article class="contact-item contact-item--main">
                            <h4 class="contact__header contact__header--main"><?php _e('Centrala', 'sag'); ?></h4>
                            <p class="contact__description contact__description--main"><?php _e('tel.:', 'sag'); ?> +48 32 255 72 60</p>
                            <p class="contact__description contact__description--main"><?php _e('fax.:', 'sag'); ?> +48 32 255 73 72</p>
                            <p class="contact__description contact__description--main"><?php _e('e-mail:', 'sag'); ?> sag@sag.pl</p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Sekretariat Zarządu', 'sag'); ?></h4>
                            <p class="contact__description"><?php _e('tel.:', 'sag'); ?> +48 728 994 663</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> sekretariat@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Dział Księgowości', 'sag'); ?></h4>
                            <p class="contact__description"><?php _e('tel.:', 'sag'); ?> +48 728 994 632</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> ksiegowosc@sag.pl</p>
                        </article>
                    </div>

                    <h3 class="contact__header--secondary"><?php _e('Dział Handlu i Marketingu', 'sag'); ?></h3>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Liny', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 994 654 Marta&nbsp;Machura</p>
                            <p class="contact__description">+48 728 352 480 Jarosław&nbsp;Sygnat</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> sag@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Krążniki', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 990 597 Józef&nbsp;Klęk</p>
                            <p class="contact__description">+48 728 990 596 Grzegorz&nbsp;Rogal</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> konstrukcje@sag.pl</p>
                            <br>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Konstrukcje', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 990 597 Józef&nbsp;Klęk</p>
                            <p class="contact__description">+48 728 352 480 Jarosław&nbsp;Sygnat</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> konstrukcje@sag.pl</p>
                        </article>
                    </div>

                    <h3 class="contact__header--secondary"><?php _e('Usługi', 'sag'); ?></h3>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Spawanie, obróbka skrawaniem, cięcie, wiercenie', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 990 597 Józef&nbsp;Klęk</p>
                            <p class="contact__description">+48 728 352 480 Jarosław&nbsp;Sygnat</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> konstrukcje@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Remonty', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 990 597 Józef&nbsp;Klęk</p>
                            <p class="contact__description">+48 728 994 634 Patrycja&nbsp;Helbin</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> konstrukcje@sag.pl</p>
                            <br class="contact-item--blankspace">
                            <br class="contact-item--blankspace">
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Serwis lin', 'sag'); ?></h4>
                            <p class="contact__description">+48 697 620 833 Tomasz&nbsp;Kokoszka</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> serwis@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4 contact-item--box">
                        <article class="contact-item ">
                            <h4 class="contact__header"><?php _e('Badania nieniszczące', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 990 599 Dariusz&nbsp;Chłond</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> serwis@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Węgiel', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 994 634 Patrycja&nbsp;Helbin</p>
                            <p class="contact__description">+48 784 964 436 Dorota&nbsp;Małobęcka</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> wegiel@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h4 class="contact__header"><?php _e('Stal', 'sag'); ?></h4>
                            <p class="contact__description">+48 728 994 641 Andrzej&nbsp;Kaczmarski</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> a.kaczmarski@sag.pl</p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <article class="contact-item">
                            <h3 class="contact__header--secondary"><?php _e('Dyrektor Produkcji', 'sag'); ?></h3>
                            <p class="contact__description">Jarosław&nbsp;Mucha +48 601 171 056</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> produkcja@sag.pl</p>
                        </article>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <h3 class="contact__header--secondary"><?php _e('Dyrektor Handlu i Marketingu', 'sag'); ?></h3>
                        <article class="contact-item">

                            <p class="contact__description">Dorota&nbsp;Małobęcka +48 784 964 436</p>
                            <p class="contact__description"><?php _e('e-mail:', 'sag'); ?> wegiel@sag.pl</p>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="contact-form-wrapper">
                        <?php if(get_bloginfo('language')=='pl-PL')
                        {
                            echo do_shortcode( '[contact-form-7 id="124" title="Formularz page-contact"]' );
                        } elseif (get_bloginfo('language')=='en-US'  || get_bloginfo('language')=='en-EN') {
                            echo do_shortcode('[contact-form-7 id="525" title="Formularz page-contact_eng"]');
                        } else echo do_shortcode('[contact-form-7 id="1787" title="Formularz page-contact_RU"]');
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="map">
                        <div id="map" class="map__container"></div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-md-4 col-md-push-1">
                <?php
                get_sidebar();
                ?>
            </div>
        </div>
    </div>
</section>

<!--Google mapy-->
<script>
        function init_Map() {
            (function (jQuery) {
                var uluru = {lat: 50.24943805, lng: 19.08165872};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 11,
                    center: uluru,
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            })(jQuery);
        }
        jQuery(document).ready(function() {
            jQuery(window).trigger("resize");
            init_Map();
        });
</script>
<?php
get_footer();?>


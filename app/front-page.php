<?php
/**
Template name: Strona główna
 */

get_header(); ?>

<?php get_template_part( 'template-parts/sections/intro' ); ?>
<?php get_template_part( 'template-parts/sections/offer' ); ?>
<?php get_template_part( 'template-parts/sections/portfolio' ); ?>
<?php get_template_part( 'template-parts/sections/news' ); ?>
<?php get_template_part( 'template-parts/sections/contact' ); ?>

    <script>
        //Google mapy
        function init_Map() {
            (function (jQuery) {
                var uluru = {lat:  50.24943805, lng: 19.08165872};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 11,
                    center: uluru,
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            })(jQuery);
        }
        jQuery(document).ready(function() {
            jQuery(window).trigger("resize");
             init_Map();
        });
    </script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJiz3EN1z0Pl18PrAQaPJKHeknWGkmRiY"></script>
<?php
get_footer(); ?>


<?php
/**
Template Name: wpis
 */

get_header(); ?>

<section class="section--full page-offer page-wpis">

    <div class="container">
        <div class="col-xs-12 col-md-7">
            <div class="col-xs-12 col-md-12">

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                ?>

                <div class="col-xs-12 col-md-12">
                    <div class="head_banner" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-xs-9 ">
                <div class="top__white">
                    <p><strong><?php the_title();?></strong></p>
<!--                    <p>Data dodania: <span>--><?php //the_date('d/m/y'); ?><!--</span></p>-->
                </div>
            </div>
        </div>



        <div class="col-xs-12 col-md-12 text_float">
            <?php the_content(); ?>



            <div class="col-xs-12 col-md-12">
                <div class="flex__container">

                    <ul class="flex-container wrap">
                        <li class="flex-item flex-item__1">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item flex-item__2">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                        <li class="flex-item">
                            <img src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                        </li>
                    </ul>
                </div>
            </div>



            <div class="col-xs-12 col-md-12 flex__container_line">
            <p class="flex__container_signature"> Kliknij na zdjęcie by powiększyć</p>
        </div>
        <hr>
        <?php
        endwhile; else: ?>
            <p><?php _e('Brak wpisów.', 'sag'); ?></p>
        <?php endif; ?>



                <!-- Left-aligned-arrow -->
                <?php
                $prev_post = get_previous_post();
                if (!empty( $prev_post )): ?>
                    <div class="col-xs-6 col-md-6 arrow_object">
                        <a class="pull-left" href="<?php echo $prev_post->guid ?>">
                            <img class="pull-left arrow_object__img__left"  <?php next_image_link(); ?> src="<?php bloginfo('template_url'); ?>/images/posts/arrow_l.png">
                        </a>
                        <span class="arrow_heading arrow_body__left"><?php posts_nav_link(); ?><?php _e('Poprzedni wpis', 'sag'); ?></span>
                    </div>
                <?php endif ?>
                <!-- Right-aligned-arrow -->
                <?php
                $next_post = get_next_post();
                if (!empty( $next_post )): ?>
                    <div class="col-xs-6 col-md-6 arrow_object">
                        <a class="pull-right" href="<?php echo get_permalink( $next_post->ID ); ?>">

                            <img class="pull-left arrow_object__img__right" src="<?php bloginfo('template_url'); ?>/images/posts/arrow_l.png">
                        </a>
                        <span class="arrow_heading arrow_body__right" <?php previous_post_link( $format, $link, $in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ); ?>><?php _e('Następny wpis', 'sag'); ?></span>
                    </div>
                <?php endif ?>



        <!-- Left-aligned -->
        <?php
        $prev_post = get_previous_post();
        if (!empty( $prev_post )): ?>
            <div class="col-xs-6 col-md-6 media_object media_object__left">
                <div class="media">
                    <a class="pull-left" href="<?php echo $prev_post->guid ?>">
                        <img class="pull-left media_object__img" src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                    </a>


                    <div class="media-body media-body__left">
                        <a class="media-heading__link" href="<?php echo $prev_post->guid ?>">
                            <h4 class="media-heading">Lewy poprzedni tytuł</h4></a>
                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <!-- Right-aligned -->
        <?php
        $next_post = get_next_post();
        if (!empty( $next_post )): ?>
            <div class="col-xs-6 col-md-6 media_object">
                <div class="media">
                    <a class="pull-right" href="<?php echo get_permalink( $next_post->ID ); ?>">
                        <img class="pull-left media_object__img" src="<?php bloginfo('template_url'); ?>/images/posts/next_thread.png">
                    </a>
                    <div class="media-body media-body__right">
                        <a class="media-heading__link" href="<?php echo get_permalink( $next_post->ID ); ?>"><h4 class="media-heading"> Prawy następny tytuł</h4></a>
                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                    </div>
                </div>
            </div>
        <?php endif ?>


        </div>
        </div>


            <!--    /*--------------Widgets----------------*/-->



    <div class="col-xs-12 col-md-4 col-md-push-1 widget-area">
        <div class="page-widget widget-contact">
            <h2 class="page-widget__title widget-contact__title">Kontakt</h2>
            <div class="media">
                <div class="media-left media-top">
                    <div class="widget-contact__image media-object"></div>
                </div>
                <div class="media-body widget-contact__content">
                    <h2 class="widget-contact__header">Lorem ipsum</h2>
                    <p class="page-widget__text widget-contact__text-small">OPERATOR CNC</p>
                    <ul class="widget-contact__list">
                        <li class="page-widget__text widget-contact__item">tel: 234 454 121</li>
                        <li class="page-widget__text widget-contact__item">mail: lorem.ipsum@sag.pl</li>
                        <li class="page-widget__text widget-contact__item">godziny pracy:</li>
                        <li class="page-widget__text widget-contact__item">4:00-23:00</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="page-widget page-widget--form widget-contact-form">
            <h2 class="page-widget__title widget-contact-form__title">Szybki Kontakt</h2>
            <p class="page-widget__text page-widget__text--center">Jeśli wolisz, wypełnij formularz kontaktowy.</p>
            <?php echo do_shortcode( '[contact-form-7 id="23" title="main-contact"]' ); ?>
        </div>

        <div class="page-widget widget-transport">
            <h2 class="page-widget__title widget-transport__title">własny transport</h2>
            <p class="page-widget__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            <div class="page-widget__btn-icon-container">
                <div class="page-widget__icon page-widget__icon--transport"></div>
                <div class="btn page-widget__btn">Dowiedz się więcej</div>
            </div>
        </div>

        <div class="page-widget page-widget--form widget-catalog">
            <h2 class="page-widget__title widget-catalog__title">Pobierz katalog</h2>
            <p class="page-widget__text page-widget__text--center widget-catalog__text">Podaj adres mail, na który chcesz otrzymać katalog produktu.</p>
            <?php echo do_shortcode( '[contact-form-7 id="24" title="main-contact"]' ); ?>
        </div>

        <div class="page-widget widget-work">
            <h2 class="page-widget__title widget-work__title">Na czym pracujemy?</h2>
            <p class="page-widget__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            <div class="page-widget__btn-icon-container">
                <div class="page-widget__icon page-widget__icon--work"></div>
                <div class="btn page-widget__btn">Dowiedz się więcej</div>
            </div>
        </div>

        <div class="page-widget widget-news">
            <h2 class="page-widget__title widget-news__title">Aktualności</h2>

            <?php $the_query = new WP_Query( array( 'cat' => 3 ) ); ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <h2 class="widget-news__title"><?php the_title(); ?></h2>
                    <div class="page-widget__text"><?php the_excerpt(); ?></div>
<!--                    <p class="page-widget__text">Data dodania: <span class="page-widget__text--green">--><?php //echo get_the_date(); ?><!--</span> </p>-->
                    <div class="btn page-widget__btn"><a href="#">Czytaj całość</a></div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
</section>
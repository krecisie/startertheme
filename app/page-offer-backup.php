<?php


get_header(); ?>

<section class="section--full page-offer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 page-offer__content">
                <h2 class="page-offer__header">Krążniki</h2>
                <p class="page-offer__text">Oferowane przez nas elementy przeznaczone są do zabudowy w przenośnikach taśmowych pracujących na powierzchni oraz do transportu urobku w podziemiach kopalń w warunkach klimatu umiarkowanego. Dostawa produktów do innych stref klimatycznych może być dokonana na podstawie uzgodnienia z wytwórcą.</p>
                <div class="page-offer__gallery">
                    <ul class="menu__round menu__round--full\">
                        <a href="#"><li class="round__item round__item--first">Kraznik X</li></a>
                        <a href="#"><li class="round__item">Kraznik X</li></a>
                        <a href="#"><li class="round__item">Kraznik X</li></a>
                        <a href="#"><li class="round__item round__item--last">Kraznik X</li></a>
                    </ul>
                </div>
                <p class="page-offer__text">Krążnik składa się z płaszcza, osi, piast (opraw łożyskowych) łożysk, elementów uszczelniających. Konstrukcja dostosowana jest do obciążeń wynikających z gęstości nasypowej 0,8-1,5 g/cm2 oraz pracy w warunkach zanieczyszczenia występującego w górnictwie węglowym. Nasze produkty odpowiadają wymaganiom normy PN-90/M-46601 i PN-91/M-46606.</p>
                <div class="page-offer__tabs">
                    <div class="cd-tabs">
                        <nav>
                            <ul class="cd-tabs-navigation">
                                <li><a data-content="1" class="selected" href="#0">Krążniki</a></li>
                                <li><a data-content="2" href="#0">Badania</a></li>
                                <li><a data-content="3" href="#0">Serwis</a></li>
                                <li><a data-content="4" href="#0">Do pobrania</a></li>
                            </ul> <!-- cd-tabs-navigation -->
                        </nav>

                        <ul class="cd-tabs-content">
                            <li class="cd-tabs-content-element selected" data-content="1">
                                <div class="description__container">
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                                <ul class="menu__round">
                                    <a href="#"><li class="round__item round__item--first">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item round__item--last">Kraznik X</li></a>
                                </ul>
                            </li>

                            <li class="cd-tabs-content-element" data-content="2">
                                <div class="description__container">
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                                <ul class="menu__round">
                                    <a href="#"><li class="round__item round__item--first">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item round__item--last">Kraznik X</li></a>
                                </ul>
                            </li>

                            <li class="cd-tabs-content-element" data-content="3">
                                <div class="description__container">
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                                <ul class="menu__round">
                                    <a href="#"><li class="round__item round__item--first">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item round__item--last">Kraznik X</li></a>
                                </ul>
                            </li>

                            <li class="cd-tabs-content-element" data-content="4">
                                <div class="description__container">
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                                <ul class="menu__round">
                                    <a href="#"><li class="round__item round__item--first">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item round__item--last">Kraznik X</li></a>
                                </ul>
                            </li>

                            <li class="cd-tabs-content-element" data-content="5">
                                <div class="description__container">
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                                <ul class="menu__round">
                                    <a href="#"><li class="round__item round__item--first">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item">Kraznik X</li></a>
                                    <a href="#"><li class="round__item round__item--last">Kraznik X</li></a>
                                </ul>
                            </li>

                        </ul> <!-- cd-tabs-content -->
                    </div> <!-- cd-tabs -->
                </div>
                <p class="page-offer__text">Firma SAG oferuje następujące typy EKO-krążników: gładkie, odciskowe, girlandowe, pierścieniowe, tarczowe oraz kierunkowe o wymiarach: średnica zewnętrzna od 55 do 159 mm; długość do 2000 mm. <strong>Elementy te są wyposażone w uszczelnienia typu labiryntowego:</strong> typu S, typu N (z odrzutnikiem), typu F (wargowe) lub na życzenie klienta simmeringi.</p>
                <p class="page-offer__text">Na produkowane przez nas krążniki <strong>udzielamy gwarancji do 3 lat</strong>, co jest potwierdzeniem zastosowania wysokiej jakości łożysk i innych elementów mających wpływ na trwałość i efektywną eksploatację.</p>
            </div>
            <div class="col-xs-12 col-md-4 widget-area">
                <div class="page-widget widget-contact">
                    <h2 class="page-widget__title widget-contact__title">Kontakt</h2>
                    <div class="media">
                        <div class="media-left media-top">
                            <div class="widget-contact__image media-object"></div>
                        </div>
                        <div class="media-body widget-contact__content">
                            <h2 class="widget-contact__header">Lorem ipsum</h2>
                            <p class="page-widget__text widget-contact__text-small">OPERATOR CNC</p>
                            <ul class="widget-contact__list">
                                <li class="page-widget__text widget-contact__item">tel: 234 454 121</li>
                                <li class="page-widget__text widget-contact__item">mail: lorem.ipsum@sag.pl</li>
                                <li class="page-widget__text widget-contact__item">godziny pracy:</li>
                                <li class="page-widget__text widget-contact__item">4:00-23:00</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="page-widget page-widget--form widget-contact-form">
                    <h2 class="page-widget__title widget-contact-form__title">Szybki Kontakt</h2>
                    <p class="page-widget__text page-widget__text--center">Jeśli wolisz, wypełnij formularz kontaktowy.</p>
                    <?php echo do_shortcode( '[contact-form-7 id="23" title="main-contact"]' ); ?>
                </div>

                <div class="page-widget widget-transport">
                    <h2 class="page-widget__title widget-transport__title">własny transport</h2>
                    <p class="page-widget__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <div class="page-widget__btn-icon-container">
                        <div class="page-widget__icon page-widget__icon--transport"></div>
                        <div class="btn page-widget__btn">Dowiedz się więcej</div>
                    </div>
                </div>

                <div class="page-widget page-widget--form widget-catalog">
                    <h2 class="page-widget__title widget-catalog__title">Pobierz katalog</h2>
                    <p class="page-widget__text page-widget__text--center widget-catalog__text">Podaj adres mail, na który chcesz otrzymać katalog produktu.</p>
                    <?php echo do_shortcode( '[contact-form-7 id="24" title="main-contact"]' ); ?>
                </div>

                <div class="page-widget widget-work">
                    <h2 class="page-widget__title widget-work__title">Na czym pracujemy?</h2>
                    <p class="page-widget__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <div class="page-widget__btn-icon-container">
                        <div class="page-widget__icon page-widget__icon--work"></div>
                        <div class="btn page-widget__btn">Dowiedz się więcej</div>
                    </div>
                </div>

                <div class="page-widget widget-news">
                    <h2 class="page-widget__title widget-news__title">Aktualności</h2>

                    <?php $the_query = new WP_Query( array( 'cat' => 3 ) ); ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <h2 class="widget-news__title"><?php the_title(); ?></h2>
                            <div class="page-widget__text"><?php the_excerpt(); ?></div>
                            <p class="page-widget__text">Data dodania: <span class="page-widget__text--green"><?php echo get_the_date(); ?></span> </p>
                            <div class="btn page-widget__btn"><a href="#">Czytaj całość</a></div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();?>
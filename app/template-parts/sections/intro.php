<section class="section--full section--intro">
    <div class="container middle">
        <div class="row">

            <div class="col-xs-12 col-md-5 col-md-offset-1">
                <img class="intro__image" src="<?php echo get_template_directory_uri();?>/images/header/sag-text.png">
                <p class="intro__text"><?php _e('Produkujemy między innymi liny stalowo-gumowe typu SAG, EKO – krążniki gładkie,
                    girlandowe, odciskowe i kierunkowe, konstrukcje do przenośników taśmowych, konstrukcje szybowe, a
                    ponadto zajmujemy się handlem węglem.', 'sag' ); ?></p>
                <p class="intro__text"><?php _e('Posiadamy również dział remontowy, który specjalizuje się w remontach hydrauliki sterowniczej oraz
                    zawieszeń.', 'sag'); ?></p>
                <p class="intro__text"><?php _e('Zapraszamy do zapoznania się z naszą ofertą.', 'sag'); ?></p>

                <a href="#offer"><span class="btn button-text btn--fl"><?php _e('Oferta', 'sag'); ?></span></a>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>o-firmie"><span class="btn button-text btn--fl btn--transparent"><?php _e('Poznaj nas', 'sag'); ?></span></a></div>

        <div class="col-xs-12 col-md-6">
            <a href="https://www.youtube.com/watch?v=_OvkVKOnIa4" class="video__link" data-lity><p class="btn btn--play btn--transparent"><span class="video__text"><?php _e('Zobacz pełne wideo', 'sag'); ?></span></p></a>
        </div>
    </div>
        <div class="row">
            <br>
<!--            <p class="intro__text--center">SAG Sp. z o.o. swoją działalność rozpoczęła 1 września 1991 roku.<br>-->
<!--                Obecnie SAG Sp. z o.o. prowadzi złożoną działalność produkcyjno-usługowo-handlową w sektorze górnictwa i przemysłu.</p>-->
        </div>
    </div> <!--container end-->

    <div class="scrolldown">
        <a href="#offer">
            <p class="scrolldown__text scrolldown--animation"><?php _e('dalej', 'sag'); ?></p>
            <div class="scrolldown__mouse">
                <span><p class="scrolldown__arrow scrolldown--animation">↓</p></span>
            </div>
        </a>
    </div>

</section>
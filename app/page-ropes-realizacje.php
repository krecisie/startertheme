<?php
/**
Template Name: Ropes-realizacje
 */
get_header(); ?>

    <section class="section--full page-offer">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-md-7 page-offer__content">
                    <div class="page-styles-default">
                        <div class="head_banner head_banner--subpage" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                            <div class="subpage__title-field">
                                <h1><?php the_title();?></h1>
                            </div>
                        </div>
                        <div class="page-offer__gallery">
                            <ul class="menu__round menu__round--full">
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 39,'page')); ?>"><li class="round__item round__item--first"><?php _e('Liny stalowo-gumowe', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 117,'page')); ?>"><li class="round__item"><?php _e('Informacje techniczne', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 120,'page')); ?>"><li class="round__item"><?php _e('Realizacje', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 122,'page')); ?>"><li class="round__item"><?php _e('Pobierz', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 113,'page')); ?>"><li class="round__item round__item--last"><?php _e('Kontakt', 'sag'); ?></li></a>
                            </ul>
                        </div>
                        <?php
                        while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                        wp_reset_query();
                        ?>

                    <div class="portfolio--flex">
                        <div class="portfolio--flex-content">
                            <ul class="slider-content nav nav-tabs owl-carousel">
                                <li data-hash="slajd-1">
                                    <h2><?php _e( 'Realizacja', 'sag'); ?> 1</h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-IMG_2663.jpg" target="_blank"><button class="btn btn--transparent-green"> <?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-2">
                                    <h2><?php _e( 'Realizacja', 'sag'); ?> 2</h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-079SAG_PD.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-3">
                                    <h2><?php _e( 'Realizacja', 'sag'); ?> 3</h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-20170529_123323.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-4">
                                    <h2><?php _e( 'Realizacja', 'sag'); ?> 4</h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-IMG_0676.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-5">
                                    <h2><?php _e( 'Realizacja', 'sag'); ?> 5</h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-IMG_2611.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-6">
                                    <h2><?php _e( 'Realizacja', 'sag'); ?> 6</h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-IMG_2615.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                            </ul>
                        </div>
                        <div class="portfolio--flex-slider">
                            <ul class="slider-images slider-images-krazniki nav nav-tabs owl-carousel" role="presentation">
                                <li role="presentation" data-hash="slajd-1">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-IMG_2663.jpg alt="Realizacje lin stalowo-gumowych" width="965px" height="535px"/>
                                    <span><?php _e( 'Realizacja', 'sag'); ?> 1</span>
                                </li>
                                <li role="presentation" data-hash="slajd-2">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/06/liny-stalowo-gumowe-realizacje-079SAG_PD-small.jpg alt="Realizacje lin stalowo-gumowych" width="965px" height="535px"/>
                                    <span><?php _e( 'Realizacja', 'sag'); ?> 2</span>
                                </li>
                                <li role="presentation" data-hash="slajd-3">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/06/liny-stalowo-gumowe-realizacje-20170529_123323-small.jpg alt="Realizacje lin stalowo-gumowych" width="965px" height="535px"/>
                                    <span><?php _e( 'Realizacja', 'sag'); ?> 3</span>
                                </li>
                                <li role="presentation" data-hash="slajd-4">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/06/liny-stalowo-gumowe-realizacje-IMG_0676-cropped.jpg alt="Realizacje lin stalowo-gumowych" width="965px" height="535px"/>
                                    <span><?php _e( 'Realizacja', 'sag'); ?> 4</span>
                                </li>
                                <li role="presentation" data-hash="slajd-5">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/05/liny-stalowo-gumowe-realizacje-IMG_2611.jpg alt="Realizacje lin stalowo-gumowych" width="965px" height="535px"/>
                                    <span><?php _e( 'Realizacja', 'sag'); ?> 5</span>
                                </li>
                                <li role="presentation" data-hash="slajd-6">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/06/liny-stalowo-gumowe-realizacje-IMG_2615-cropped.jpg alt="Realizacje lin stalowo-gumowych" width="965px" height="535px"/>
                                    <span><?php _e( 'Realizacja', 'sag'); ?> 6</span>
                                </li>
                            </ul>
                            <div class="slider-menu--nav" role="navigation">
                                <button class="arrow left">
                                    <svg viewBox="0 0 50 80" xml:space="preserve">
<polyline fill="none" stroke="#1a1a1a" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
45.63,75.8 0.375,38.087 45.63,0.375 "/>
</svg>
                                </button>
                                <button class="arrow right">
                                    <svg viewBox="0 0 50 80" xml:space="preserve">
<polyline fill="none" stroke="#1a1a1a" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
0.375,0.375 45.63,38.087 0.375,75.8 "/>
</svg>
                                </button>
                            </div>
                        </div>
                    </div>

<!--                        <div class="page-offer__gallery">-->
<!--                            <ul class="menu__round menu__round--full">-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 39,'page')); ?><!--"><li class="round__item round__item--first">--><?php //_e('Liny stalowo-gumowe', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 117,'page')); ?><!--"><li class="round__item">--><?php //_e('Informacje techniczne', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 120,'page')); ?><!--"><li class="round__item">--><?php //_e('Realizacje', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 122,'page')); ?><!--"><li class="round__item">--><?php //_e('Pobierz', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 113,'page')); ?><!--"><li class="round__item round__item--last">--><?php //_e('Kontakt', 'sag'); ?><!--</li></a>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-md-push-1">
                    <?php
                    get_sidebar();
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();?>
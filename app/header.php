<?php

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-102429269-1', 'auto');
        ga('send', 'pageview');

    </script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,700&amp;subset=latin-ext" rel="stylesheet">
    <script type="application/ld+json">
        {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",
        "address": {
        "@type": "PostalAddress",
        "addressLocality": "Katowice",
        "streetAddress": "Szopienicka 58a",
        "postalCode": "40-432",
        "addressRegion": "śląskie"
        },
        "name": "SAG Sp. z o.o.",
        "email": "sag@sag.pl",
        "telephone": "322557260",
        "vatID": "6340130472",
        "image": "http://sag.pl/wp-content/themes/app/images/header/sag-logo.png"
        }
    </script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php get_template_part( 'template-parts/components/menu' ); ?>

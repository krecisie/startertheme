(function(jQuery){
    "use strict";

    jQuery(window).load(function() {
        jQuery(window).trigger("resize");
    });

    jQuery(document).ready(function() {
        jQuery(window).trigger("resize");
        init_sectionFull();
        init_menuDropdownOnHover();
        init_OwlCarousel();
        init_SmoothScroll();
        jQuery( '.menu-item-has-children' ).doubleTapToGo();

    });

    jQuery(window).resize(function() {
        close_menu();
        init_sectionFull();
    });

})(jQuery);

// Full height section
function init_sectionFull()
{
    var viewportHeight = jQuery(window).height();
    jQuery(".section--full").css({'min-height': viewportHeight});
}
// End of height section

// Smooth scroll
function init_SmoothScroll() {
    jQuery('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html, body').animate({
                    scrollTop: target.offset().top - jQuery("#main-navbar").outerHeight(false)
                }, 450);
                return false;
            }
        }
    });
}
// End of smooth scroll

//================================//
//Fullscreen navbar on mobile & tablet

    function changeClass()
    {
        document.getElementById('main-navbar').classList.toggle('navbar--full');
    }

    window.onload = function()
    {
        document.getElementById("main-navbar__button").addEventListener( 'click', changeClass);
    }
//End of fullscreen navbar on mobile & tablet

//Closing menu on window resize
    function close_menu() {
        document.getElementById('main-navbar').classList.remove('navbar--full');
        jQuery('.navbar-collapse').collapse('hide');
    }
//End of closing menu on window resize

function init_OwlCarousel() {

    jQuery('.slider-menu').owlCarousel({
        nav: false,
        dots: false,
        loop: true,
        center: true,
        mouseDrag: false,
        touchDrag: true,
        responsive:{
            0:{
                items: 1,
                margin: 0,
                stagePadding: 0,
            },
            450:{
                items: 2,
                margin: 55,
                stagePadding: 100,
            },
            600:{
                items: 3,
		        margin: 70,
                stagePadding: 150,
            },
            1000:{
                items: 3,
		        margin: 50,
                stagePadding: 50,
            }
        }
        // autoplay:true
        // autoplayTimeout:5000
    });

    jQuery('.loop').owlCarousel({
        center: true,
        items:1,
        loop:true,
        margin:34,
        autoWidth:true,
        onDrag:navDown,
        onTranslate:navDown,
        onTranslated:navUp,
        responsive:{
            769:{
                items:6,
                margin:0
            }
        }
    });

    jQuery('.slider-images').owlCarousel({
        nav: false,
        dots: false,
        loop: true,
        center: true,
        mouseDrag: false,
        touchDrag: true,
        lazyLoad: true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        margin: 0,
        items: 1
        // autoplay:true
        // autoplayTimeout:5000
    });

    jQuery('.slider-content').owlCarousel({
        nav: false,
        dots: false,
        loop: true,
        center: true,
        mouseDrag: false,
        touchDrag: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        margin: 0,
        items: 1
        // autoplay:true
        // autoplayTimeout:5000
    });

    jQuery('.clients-logos__slider').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:6000,
        responsive: {
            0: {
                center:true,
                items:2,
            },
            480: {
                center:true,
                items:3,
            },
            1200: {
                center:false,
                items:5,
            }
        }
    });

    var sliderMenu = jQuery('.slider-menu').owlCarousel();
    var sliderImages = jQuery('.slider-images').owlCarousel();
    var newsCarousel = jQuery('.news-carousel').owlCarousel();

    jQuery(".slider-menu--nav .left").click(function () {
        sliderMenu.trigger('prev.owl.carousel');
        sliderImages.trigger('prev.owl.carousel');
    });

    jQuery(".slider-menu--nav .right").click(function () {
        sliderMenu.trigger('next.owl.carousel');
        sliderImages.trigger('next.owl.carousel');
    });

    jQuery(".news__nav-btn--left").click(function () {
        newsCarousel.trigger('prev.owl.carousel');
    });

    jQuery(".news__nav-btn--right").click(function () {
        newsCarousel.trigger('next.owl.carousel');
    });

    function navDown(event) {
        jQuery(".news__nav-btn").addClass("button-inactive");
    };
    function navUp(event) {
        jQuery(".news__nav-btn").removeClass("button-inactive");
    };
}

// Menu dropdown on hover instead of on click
function init_menuDropdownOnHover() {
    (function($) {
        $(function(){
            $('.dropdown').hover(function() {
                    $(this).addClass('open');
                },
                function() {
                    $(this).removeClass('open');
                });
        });
    })( jQuery );
}
//End of menu dropdown on hover instead of on click

//Menu item with children needs to be touched two times to open the link
;(function( $, window, document, undefined )
{
jQuery.fn.doubleTapToGo = function( params )
{
    if( !( 'ontouchstart' in window ) &&
        !navigator.msMaxTouchPoints &&
        !navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;

    this.each( function()
    {
        var curItem = false;

        jQuery( this ).on( 'click', function( e )
        {
            var item = $( this );
            if( item[ 0 ] != curItem[ 0 ] )
            {
                e.preventDefault();
                curItem = item;
            }
        });

        jQuery( document ).on( 'click touchstart MSPointerDown', function( e )
        {
            var resetItem = true,
                parents	  = $( e.target ).parents();

            for( var i = 0; i < parents.length; i++ )
                if( parents[ i ] == curItem[ 0 ] )
                    resetItem = false;

            if( resetItem )
                curItem = false;
        });
    });
    return this;
};
})( jQuery, window, document );
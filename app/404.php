<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sag
 */

get_header(); ?>

    <section class="error-404 not-found section--full">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="page-styles-default">
                        <header class="page-header">
                            <h1 class="page-title"><?php esc_html_e( '404 - strony nie znaleziono', 'sag' ); ?></h1>
                        </header><!-- .page-header -->
                        <div class="page-content">
                            <?php
                                the_widget( 'WP_Widget_Recent_Posts' );
                            ?>
                        </div><!-- .page-content -->
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-md-push-1">
                    <?php
                    dynamic_sidebar( 'widget-404' );
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();

<?php
/**
Template name: Machines
 */

get_header(); ?>

    <section class="section--full page-offer">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-md-7 page-offer__content">
                    <div class="page-styles-default">
                        <div class="head_banner head_banner--subpage" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                            <div class="subpage__title-field">
                                <h1><?php the_title();?></h1>
                            </div>
                        </div>
                        <?php
                        while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-md-push-1">
                    <?php
                    get_sidebar();
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();?>
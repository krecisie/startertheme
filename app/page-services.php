<?php
/**
Template name: Services
 */

get_header(); ?>

    <section class="section--full page-offer">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7 page-offer__content">
                    <div class="page-styles-default">
                        <div class="head_banner head_banner--subpage" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                            <div class="subpage__title-field">
                                <h1><?php the_title();?></h1>
                            </div>
                        </div>
                        <div class="page-offer__gallery">
                            <ul class="menu__round menu__round--services menu__round--full">
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 179,'page')); ?>"><li class="round__item round__item--first"><?php _e('Usługi', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 191,'page')); ?>"><li class="round__item"><?php _e('Cięcie i gięcie blach i profili', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 204,'page')); ?>"><li class="round__item"><?php _e('Obróbka skrawaniem', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 207,'page')); ?>"><li class="round__item"><?php _e('Spawanie ręczne i zrobotyzowane', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 275,'page')); ?>"><li class="round__item"><?php _e('Remonty hydrauliki sterowniczej i zawieszeń szybowych', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 280,'page')); ?>"><li class="round__item"><?php _e('Badania nieniszczące NDT', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 1010,'page')); ?>"><li class="round__item round__item--last"><?php _e('Kontakt', 'sag'); ?></li></a>
                            </ul>
                        </div>
                        <?php
                        while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                        wp_reset_query();
                        ?>
                        <div class="page-offer__gallery">
                            <ul class="menu__round menu__round--services menu__round--full">
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 179,'page')); ?>"><li class="round__item round__item--first"><?php _e('Usługi', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 191,'page')); ?>"><li class="round__item"><?php _e('Cięcie i gięcie blach i profili', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 204,'page')); ?>"><li class="round__item"><?php _e('Obróbka skrawaniem', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 207,'page')); ?>"><li class="round__item"><?php _e('Spawanie ręczne i zrobotyzowane', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 275,'page')); ?>"><li class="round__item"><?php _e('Remonty hydrauliki sterowniczej i zawieszeń szybowych', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 280,'page')); ?>"><li class="round__item"><?php _e('Badania nieniszczące NDT', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 1010,'page')); ?>"><li class="round__item round__item--last"><?php _e('Kontakt', 'sag'); ?></li></a>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 col-md-push-1">
                    <?php
                    get_sidebar();
                    ?>
                </div>
            </div>
        </div>
        </div>
    </section>

<?php
get_footer();?>
<?php
/**
 * The template for displaying archive of news
 * @package sag
 */

get_header(); ?>

<section class="section--full subpage-news menu-subpage">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7 subpage-news__content">
                <h1 class="subpage-news__header"><?php _e( 'Aktualności', 'sag'); ?></h1>


                <?php
                // set the "paged" parameter (use 'page' if the query is on a static front page)
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                // the query
                $the_query = new WP_Query( 'cat=5&paged=' . $paged );
                ?>

                <?php if ( $the_query->have_posts() ) : ?>
                    <?php
//Protect against arbitrary paged values
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

                    $args = array(
                        'category_name' => 'news',
                        'paged' => $paged,
                    );

                    $the_query = new WP_Query( $args );
                    ?>
                    <?php

// the loop
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        ?>
                        <div class="subpage-news__entry">
                            <h2 class="subpage-news__entry__title"><?php the_title(); ?></h2>
<!--                            <p class="subpage-news__entry__date">Data dodania: <span class="page-widget__text--green">--><?php //echo get_the_date('d/m/Y'); ?><!--</span> </p>-->
                            <div class="subpage-news__entry__image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                            <div class="subpage-news__entry__text"><?php the_excerpt(); ?></div>
                            <div class="btn-container">
                                <a class="btn btn--alternate-hover subpage-news__entry__btn" href="<?php the_permalink(); ?>"><?php _e( 'Czytaj całość', 'sag'); ?></a>
                            </div>

                        </div>
                    <?php endwhile; ?>
                    <?php
                    $big = 999999999; // need an unlikely integer
?>
                    <div class="subpage-news__pagination">
                        <?php
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'mid_size' => 3,
                            'total' => $the_query->max_num_pages,
                            'prev_text' => '<span class="subpage-news__arrow subpage-news__arrow--previous"></span>',
                            'next_text' => '<span class="subpage-news__arrow subpage-news__arrow--next"></span>'
                        ) );
                        ?>
                    </div>

                    <?php
// clean up after the query and pagination
                    wp_reset_postdata();
                    ?>

                <?php else:  ?>
                    <p><?php _e( 'Przepraszamy, brak wpisów spełniających podane kryteria.', 'sag'); ?></p>
                <?php endif; ?>

            </div>
            <div class="col-xs-12 col-md-4 col-md-push-1">
                <?php
                dynamic_sidebar( 'widget-news' );
                ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();

<section class="section--full contact">
    <div class="contact__section-header section-header">
        <h1 class="section-header__text section-header__text--big"><?php _e('Kontakt', 'sag'); ?></h1>
        <h1 class="section-header__text section-header__text--small"><?php _e('Kontakt', 'sag'); ?></h1>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="tel">
                    <ul class="tel__list">
                        <li class="tel__item"><p class="tel__item--bold"><?php _e('Centrala', 'sag'); ?></p></li>
                        <li class="tel__item"><?php _e('tel.:', 'sag'); ?> <p class="tel__item--bold">+48 32 255 72 60</p></li>
                        <li class="tel__item"><?php _e('fax.:', 'sag'); ?> <p class="tel__item--bold">+48 32 255 73 72</p></li>
                        <li class="tel__item"><?php _e('e-mail:', 'sag'); ?> <p class="tel__item--bold">sag@sag.pl</p></li>
                    </ul>
                </div>
                <?php if(get_bloginfo('language')=='pl-PL')
                {
                    echo do_shortcode( '[contact-form-7 id="119" title="Formularz main-page"]' );
                } elseif (get_bloginfo('language')=='en-US'  || get_bloginfo('language')=='en-EN') {
                    echo do_shortcode('[contact-form-7 id="524" title="Formularz main-page_eng"]');
                } else echo  do_shortcode('[contact-form-7 id="1786" title="Formularz main-page RU"]');
                ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="map">
                    <div id="map" class="map__container"></div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php


if ( ! function_exists( 'sag_setup' ) ) :

function sag_setup() {
	/*
	 * Translations can be filed in the /languages/ directory.
	 */
	load_theme_textdomain( 'sag', get_template_directory() . '/languages' );


	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.

    register_nav_menus( array(
        'menu-1' => esc_html__( 'Primary', 'sag' )
    ) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );


	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'sag_setup' );

//function sag_content_width() {
//	$GLOBALS['content_width'] = apply_filters( 'sag_content_width', 640 );
//}
//add_action( 'after_setup_theme', 'sag_content_width', 0 );


function register_kariera() {

    /**
     * Post Type: Slaider.
     */

    $labels = array(
        "name" => __( 'Kariera', 'cashbroker' ),
        "singular_name" => __( 'Oferta', 'cashbroker' ),
        "menu_name" => __( 'Oferty pracy', 'cashbroker' ),
        "all_items" => __( 'Wszystkie oferty', 'cashbroker' ),
        "add_new" => __( 'Dodaj ofertę', 'cashbroker' ),
        "add_new_item" => __( 'Dodaj ofertę', 'cashbroker' ),
        "edit_item" => __( 'Edytuj ofertę', 'cashbroker' ),
        "new_item" => __( 'Nowa oferta', 'cashbroker' ),
        "view_item" => __( 'Zobacz ofertę', 'cashbroker' ),
        "view_items" => __( 'Zobacz oferty', 'cashbroker' ),
        "search_items" => __( 'Szukaj', 'cashbroker' ),
        "not_found" => __( 'Nie znaleziono', 'cashbroker' ),
        "not_found_in_trash" => __( 'Nie znaleziono', 'cashbroker' ),
        "parent_item_colon" => __( 'Rodzic', 'cashbroker' ),
        "featured_image" => __( 'Obrazek wyróżniający', 'cashbroker' ),
        "set_featured_image" => __( 'Ustaw obrazek wyróżniający', 'cashbroker' ),
        "remove_featured_image" => __( 'Usuń obrazek wyróżniający', 'cashbroker' ),
        "use_featured_image" => __( 'Użyj obrazka wyróżniającego', 'cashbroker' ),
        "archives" => __( 'Archiwa', 'cashbroker' ),
        "insert_into_item" => __( 'Wstaw do wpisu', 'cashbroker' ),
        "uploaded_to_this_item" => __( 'Wrzucone do tej oferty', 'cashbroker' ),
        "filter_items_list" => __( 'Filtruj', 'cashbroker' ),
        "items_list_navigation" => __( 'Nawigacja', 'cashbroker' ),
        "items_list" => __( 'Lista ofert', 'cashbroker' ),
        "attributes" => __( 'Atrybuty', 'cashbroker' ),
        "parent_item_colon" => __( 'Rodzic', 'cashbroker' ),
    );

    $args = array(
        "label" => __( 'Kariera', 'cashbroker' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "kariera", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "dashicons-businessman",
        "supports" => array( "thumbnail", "editor", "title" ),
    );

    register_post_type( "kariera", $args );
}



add_action( 'init', 'register_kariera' );


/**
 * Register widget area.
 */
function sag_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sag' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sag' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sag_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sag_scripts() {
    //	Bootstrap
    wp_enqueue_style( 'bootstrap-css',  get_template_directory_uri() . '/styles/bootstrap.css' );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/scripts/bootstrap.js', array('jquery'), true );
	wp_enqueue_style( 'sag-style', get_stylesheet_uri() );
    wp_enqueue_style( 'main-css',  get_template_directory_uri() . '/styles/main.css' );
    // Animate.css
    wp_enqueue_style( 'animate-css',  get_template_directory_uri() . '/styles/animate.css' );
    // Owl.carousel - carousel
    wp_enqueue_script( 'owl-js',  get_template_directory_uri() . '/scripts/vendor/owl.carousel.js', array('jquery'), true  );
    wp_enqueue_style( 'owl-main',  get_template_directory_uri() . '/styles/vendor/owl.carousel.css', 'owl-js', true   );
    wp_enqueue_style( 'owl-theme',  get_template_directory_uri() . '/styles/vendor/owl.theme.default.css', 'owl-main', true   );
    
    // Lity - lightbox for video
    wp_enqueue_script( 'lity-js', get_template_directory_uri() . '/scripts/vendor/lity.min.js', array('jquery'), true );
    wp_enqueue_style( 'lity-css',  get_template_directory_uri() . '/styles/vendor/lity.min.css' );

	// Bootstrap FileStyle
    wp_enqueue_script( 'bootstrap-filestyle.min.js', get_template_directory_uri() . '/scripts/vendor/bootstrap-filestyle.min.js', array('jquery'), true );



    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/scripts/main.js', array('jquery'), true );
    wp_enqueue_script( 'tabs-js', get_template_directory_uri() . '/scripts/vendor/tabs.js', array('jquery'), true );
}
add_action( 'wp_enqueue_scripts', 'sag_scripts' );

function custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


//Remove paragraph tag around images in content
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
//End of remove paragraph tag around images in content


//Adding wrapper around images in content
function filter_images($content){
    return preg_replace('/<img (.*) \/>\s*/iU', '<span class="image-wrap"><img \1 /></span>', $content);
}
add_filter('the_content', 'filter_images');
//End of adding wrapper around images in content

/* Theme setup */
add_action( 'after_setup_theme', 'wpt_setup' );
if ( ! function_exists( 'wpt_setup' ) ):
    function wpt_setup() {
        register_nav_menus(
            array(
                'primary' => __( 'Primary navigation', 'wptuts' ),
                'footer-menu' => __( 'Footer menu' )
            )
        );
    } endif;

require_once('wp-bootstrap-navwalker.php');

add_action( 'pre_get_posts',  'set_posts_per_page'  );
function set_posts_per_page( $query ) {
    global $wp_the_query;
    if ( is_category( 'news' )) {
        $query->set( 'posts_per_page', 4 );
    }
    return $query;
}




//WIDGETY

class widget_contact extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'widget_contact',
            __('W Kontakt', 'widget_contact_domain'),
            array('description' => __('Widget kontakt', 'widget_contact_domain'),)
        );
    }

    public function widget($args, $instance)
    {
        $name = apply_filters('widget_contact_name', $instance['name']);
        $position = apply_filters('widget_contact_position', $instance['position']);
        $phone = apply_filters('widget_contact_phone', $instance['phone']);
        $mail = apply_filters('widget_contact_mail', $instance['mail']);
        $hours = apply_filters('widget_contact_hours', $instance['hours']);
        ?>
        <div class="page-widget widget-contact">
            <h2 class="page-widget__title widget-contact__title"><?php _e('Kontakt', 'sag'); ?></h2>
            <div class="media">
                <div class="media-left media-top">
                    <div class="widget-contact__image media-object"></div>
                </div>
                <div class="media-body widget-contact__content">
                    <h2 class="widget-contact__header"><?php echo $name;?></h2>
                    <p class="page-widget__text widget-contact__text-small"><?php echo $position;?></p>
                    <ul class="widget-contact__list">
                        <li class="page-widget__text widget-contact__item">tel: <?php echo $phone;?></li>
                        <li class="page-widget__text widget-contact__item">mail: <?php echo $mail;?></li>
                        <li class="page-widget__text widget-contact__item"><?php _e('godziny pracy:', 'sag'); ?></li>
                        <li class="page-widget__text widget-contact__item"><?php echo $hours;?></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }

    public function form($instance) {

        if (isset($instance['name'])) { $name = $instance['name']; }
        else { $name = __( 'Imię i nazwisko', 'widget_contact_domain' ); }?>
        <p> <label for="<?php echo $this->get_field_id( 'name' ); ?>"><?php _e( 'Imię i nazwisko:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>" /> </p>
        <?php

        if (isset($instance['position'])) { $position = $instance['position']; }
        else { $position = __( 'Stanowisko', 'widget_contact_domain' ); }?>
        <p> <label for="<?php echo $this->get_field_id( 'position' ); ?>"><?php _e( 'Stanowisko:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'position' ); ?>" name="<?php echo $this->get_field_name( 'position' ); ?>" type="text" value="<?php echo esc_attr( $position ); ?>" /></p>
        <?php

        if (isset($instance['phone'])) { $phone = $instance['phone']; }
        else { $phone = __( 'XXX XXX XXX', 'widget_contact_domain' ); }?>
        <p> <label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Numer telefonu:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>" /></p>
        <?php

        if (isset($instance['mail'])) { $mail = $instance['mail']; }
        else { $mail = __( 'sag@sag.pl', 'widget_contact_domain' ); }?>
        <p><label for="<?php echo $this->get_field_id( 'mail' ); ?>"><?php _e( 'E-mail:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'mail' ); ?>" name="<?php echo $this->get_field_name( 'mail' ); ?>" type="text" value="<?php echo esc_attr( $mail ); ?>" /></p>
        <?php

        if (isset($instance['hours'])) { $hours = $instance['hours']; }
        else { $hours = __( '8:00-16:00', 'widget_contact_domain' ); }?>
        <p> <label for="<?php echo $this->get_field_id( 'hours' ); ?>"><?php _e( 'Godziny pracy:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'hours' ); ?>" name="<?php echo $this->get_field_name( 'hours' ); ?>" type="text" value="<?php echo esc_attr( $hours ); ?>" /></p>
        <?php

    }
}

class widget_contact_form extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'widget_contact_form',
            __('W Formularz kontaktowy', 'widget_contact_form_domain'),
            array('description' => __('Widget formularz kontaktowy', 'widget_contact_form_domain'),)
        );
    }
    public function widget( $args, $instance ) {
        ?>
			<div class="page-widget page-widget--form widget-contact-form">
				<h2 class="page-widget__title widget-contact-form__title"><?php _e('Szybki Kontakt', 'sag'); ?></h2>
				<p class="page-widget__text page-widget__text--center"><?php _e('Jeśli wolisz, wypełnij formularz kontaktowy.', 'sag'); ?></p>
                <?php if(get_bloginfo('language')=='pl-PL')
                {
                    echo do_shortcode( '[contact-form-7 id="125" title="Formularz widget-contact-form"]' );
                } elseif (get_bloginfo('language')=='en-US'  || get_bloginfo('language')=='en-EN') {
                    echo do_shortcode(' [contact-form-7 id="527" title="Formularz widget-contact-form_eng"]');
                } else echo do_shortcode(' [contact-form-7 id="1796" title="Formularz widget-contact-form_RU"]');
                ?>
			</div>
        <?php
    }
}

class widget_transport extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'widget_transport',
            __('W Transport', 'widget_transport_domain'),
            array('description' => __('Widget transport', 'widget_transport_domain'),)
        );
    }
    public function widget( $args, $instance ) {
    	?>
			<div class="page-widget widget-transport">
				<h2 class="page-widget__title widget-transport__title"><?php _e('własny transport', 'sag'); ?></h2>
				<p class="page-widget__text"><?php _e('Dysponujemy własnym transportem - ciągnik siodłowy z naczepą o ładowności 24t.', 'sag'); ?></p>
				<div class="page-widget__btn-icon-container">
					<div class="page-widget__icon page-widget__icon--transport"></div>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>services/transport-gratis"><div class="btn page-widget__btn"><?php _e('Dowiedz się więcej', 'sag'); ?></div></a>
				</div>
			</div>
        <?php
    }
}

class widget_catalog extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'widget_catalog',
            __('W Pobierz katalog', 'widget_catalog_domain'),
            array('description' => __('Widget pobierz katalog', 'widget_catalog_domain'),)
        );
    }
    public function widget( $args, $instance ) {
        ?>
		<div class="page-widget page-widget--form widget-catalog">
			<h2 class="page-widget__title widget-catalog__title"><?php _e('Pobierz katalog', 'sag'); ?></h2>
			<p class="page-widget__text page-widget__text--center widget-catalog__text"><?php _e('Podaj adres mail, na który chcesz otrzymać katalog produktu.', 'sag'); ?></p>
            <?php if(get_bloginfo('language')=='pl-PL')
            {
                echo do_shortcode( '[contact-form-7 id="126" title="Formularz widget-catalog"]' );
            } elseif (get_bloginfo('language')=='en-US'  || get_bloginfo('language')=='en-EN') {
                echo do_shortcode('[contact-form-7 id="526" title="Formularz widget-catalog_eng"]');
            } else echo do_shortcode('[contact-form-7 id="1795" title="Formularz widget-catalog_RU"]');
            ?>
		</div>
        <?php
    }
}

class widget_work extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'widget_work',
            __('W Na czym pracujemy', 'widget_work_domain'),
            array('description' => __('Widget na czym pracujemy', 'widget_work_domain'),)
        );
    }
    public function widget( $args, $instance ) {
        ?>
		<div class="page-widget widget-work">
			<h2 class="page-widget__title widget-work__title"><?php _e('', 'sag'); ?></h2>
			<p class="page-widget__text"><?php _e('', 'sag'); ?></p>
			<div class="page-widget__btn-icon-container">
				<div class="page-widget__icon page-widget__icon--work"></div>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>park-maszynowy"><div class="btn page-widget__btn"><?php _e('', 'sag'); ?></div></a>
			</div>
		</div>
        <?php
    }
}

class widget_news extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'widget_news',
            __('W Aktualności', 'widget_news_domain'),
            array('description' => __('Widget aktualności', 'widget_news_domain'),)
        );
    }
    public function widget( $args, $instance ) {
        ?>
		<div class="page-widget widget-news">
			<h2 class="page-widget__title widget-news__title"><?php _e('Aktualności', 'sag'); ?></h2>
            <?php $the_query = new WP_Query( array( 'cat' => 5, 'posts_per_page'   => 3 ) ); ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="media widget-news__wrapper">
						<div class="media-left media-top">
							<div class="media-object widget-news__thumbnail" style="background-image: url('<?php echo the_post_thumbnail_url() ;?>')"></div>
						</div>
						<div class="media-body widget-news__content">
							<h2 class="widget-news__header"><?php the_title(); ?></h2>
							<div class="page-widget__text widget-news__text"><?php the_excerpt(); ?></div>
						</div>
						<div class="widget-news__bottom">
<!--							<p class="page-widget__text widget-news__date-wrapper">--><?php //_e('Data dodania: ', 'sag'); ?><!--<span class="widget-news__text--date page-widget__text--green">--><?php //echo get_the_date("m.d.Y"); ?><!--</span> </p>-->
							<div class="btn page-widget__btn"><a href="<?php the_permalink(); ?>"><?php _e('Czytaj całość', 'sag'); ?></a></div>
						</div>
					</div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
		</div>
        <?php
    }
}

class widget_products extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'widget_products',
            __('W Produkty i usługi', 'widget_products_domain'),
            array('description' => __('Widget produkty i usługi', 'widget_products_domain'),)
        );
    }
    public function widget( $args, $instance ) {
        ?>
		<div class="page-widget widget-products">
			<h2 class="page-widget__title widget-products__title"><?php _e('Produkty i usługi', 'sag'); ?></h2>
            <div class="media widget-products__wrapper">
                <div class="media-left media-top">
                    <div class="media-object widget-products__thumbnail" style="background-image: url('<?php bloginfo('template_url'); ?>/images/widgets/liny-image-thumb.jpg"></div>
                </div>
                <div class="media-body widget-products__content">
                    <h2 class="widget-products__header"><?php _e('Liny stalowo-gumowe', 'sag'); ?></h2>
                    <div class="page-widget__text widget-products__text"><?php _e('Liny płaskie stalowo-gumowe typu SAG cechują się dużą niezawodnością i bezpieczeństwem pracy, a także kilkakrotnie dłuższym okresem eksploatacji […]', 'sag'); ?></div>
                </div>
                <div class="widget-products__bottom">
                    <div class="btn page-widget__btn"><a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 39,'page')); ?>"><?php _e('Zobacz więcej', 'sag'); ?></a></div>
                </div>
            </div>

            <div class="media widget-products__wrapper">
                <div class="media-left media-top">
                    <div class="media-object widget-products__thumbnail" style="background-image: url('<?php bloginfo('template_url'); ?>/images/widgets/krazniki-image-thumb.jpg"></div>
                </div>
                <div class="media-body widget-products__content">
                    <h2 class="widget-products__header"><?php _e('Krążniki', 'sag'); ?></h2>
                    <div class="page-widget__text widget-products__text"><?php _e('Firma SAG oferuje następujące typy EKO-krążników: gładkie, odciskowe, girlandowe, pierścieniowe, tarczowe oraz kierunkowe […]', 'sag'); ?></div>
                </div>
                <div class="widget-products__bottom">
                    <div class="btn page-widget__btn"><a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 2,'page')); ?>"><?php _e('Zobacz więcej', 'sag'); ?></a></div>
                </div>
            </div>

            <div class="media widget-products__wrapper">
                <div class="media-left media-top">
                    <div class="media-object widget-products__thumbnail" style="background-image: url('<?php bloginfo('template_url'); ?>/images/widgets/konstrukcje-image-thumb.jpg"></div>
                </div>
                <div class="media-body widget-products__content">
                    <h2 class="widget-products__header"><?php _e('Konstrukcje', 'sag'); ?></h2>
                    <div class="page-widget__text widget-products__text"><?php _e('Firma SAG produkuje konstrukcje stalowe ze stali konstrukcyjnych we wszystkich gatunkach. Wytwarzane produkty przeznaczone są dla przemysłu budowlanego i energetyki[…]', 'sag'); ?></div>
                </div>
                <div class="widget-products__bottom">
                    <div class="btn page-widget__btn"><a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 56,'page')); ?>"><?php _e('Zobacz więcej', 'sag'); ?></a></div>
                </div>
            </div>

            <div class="media widget-products__wrapper">
                <div class="media-left media-top">
                    <div class="media-object widget-products__thumbnail" style="background-image: url('<?php bloginfo('template_url'); ?>/images/widgets/uslugi-image-thumb.jpg"></div>
                </div>
                <div class="media-body widget-products__content">
                    <h2 class="widget-products__header"><?php _e('Usługi', 'sag'); ?></h2>
                    <div class="page-widget__text widget-products__text"><?php _e('Firma SAG przy użyciu swojego parku maszynowego świadczy usługi w następującym zakresie: cięcie i gięcie blach i profili, obróbka skrawaniem[…]', 'sag'); ?></div>
                </div>
                <div class="widget-products__bottom">
                    <div class="btn page-widget__btn"><a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 179,'page')); ?>"><?php _e('Zobacz więcej', 'sag'); ?></a></div>
                </div>
            </div>

		</div>
        <?php
    }
}

function load_widget() {
    register_widget( 'widget_contact' );
    register_widget( 'widget_contact_form' );
    register_widget( 'widget_transport' );
    register_widget( 'widget_catalog' );
    register_widget( 'widget_work' );
    register_widget( 'widget_news' );
    register_widget( 'widget_products' );
}
add_action( 'widgets_init', 'load_widget' );
?>

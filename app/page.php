<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sag
 */

get_header(); ?>

<section class="section--full page-offer">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7 page-offer__content">
                <div class="page-styles-default">
                    <?php
                    while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-md-push-1">
                <?php
                get_sidebar();
                ?>
            </div>
        </div>
    </div>

</section>

<?php
get_footer();?>
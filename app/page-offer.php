<?php
/**
Template name: Offer
 */

get_header(); ?>

<section class="section--full page-offer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-7 page-offer__content">
                <h2 class="page-offer__header"><?php the_title(); ?></h2>
                <div class="page-offer__gallery">
                    <ul class="menu__round menu__round--full">
                        <a href="#"><li class="round__item round__item--first"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item round__item--last"><?php _e('', 'sag'); ?></li></a>
                    </ul>
                </div>
                <?php
                while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
                <div class="page-offer__gallery">
                    <ul class="menu__round menu__round--full">
                        <a href="#"><li class="round__item round__item--first"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item"><?php _e('', 'sag'); ?></li></a>
                        <a href="#"><li class="round__item round__item--last"><?php _e('', 'sag'); ?></li></a>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-md-push-1">
                <?php
                get_sidebar();
                ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();?>
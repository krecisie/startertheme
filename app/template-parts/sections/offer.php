<section class="section--full offer" id="offer">
<div class="container">
    <div class="news__section-header section-header">
        <h1 class="section-header__text section-header__text--big"><?php _e('Oferta', 'sag'); ?></h1>
        <h1 class="section-header__text section-header__text--small"><?php _e('Oferta', 'sag'); ?></h1>
    </div>
        <div class="cd-tabs">
            <nav>
                <ul class="cd-tabs-navigation">
                    <li><a data-content="1" class="selected" href="#0"><?php _e('Liny stalowo-gumowe', 'sag'); ?></a></li>
                    <li><a data-content="2" href="#0"><?php _e('Krążniki', 'sag'); ?></a></li>
                    <li><a data-content="3" href="#0"><?php _e('Konstrukcje', 'sag'); ?></a></li>
                    <li><a data-content="4" href="#0"><?php _e('Usługi', 'sag'); ?></a></li>
                    <li><a data-content="5" href="#0"><?php _e('Handel', 'sag'); ?></a></li>
                </ul> <!-- cd-tabs-navigation -->
            </nav>

            <ul class="cd-tabs-content">
                <li class="cd-tabs-content-element selected" data-content="1">
                    <div class="description__container">
                        <article class="description">
                            <h2 class="header--underline"><?php _e('Historia lin stalowo-gumowych', 'sag'); ?></h2>
                            <p class="description__text"><?php _e('W 1975 roku rozpoczęto próby z&nbsp;nową konstrukcją lin wyrównawczych płaskich stalowo-gumowych SAG, aby rozwiązać szereg problemów związanych z&nbsp;systemem transportu pionowego. W&nbsp;szczególności chodziło o:', 'sag'); ?></p>
                            <ul class="description__list">
                                <li><?php _e('zapewnienie linie wyrównawczej odporności na działanie korozji przy zachowaniu wymaganego współczynnika bezpieczeństwa,', 'sag'); ?></li>
                                <li><?php _e('zlikwidowanie częstych, trudnych i&nbsp;kosztownych wymian tradycyjnych lin wyrównawczych,', 'sag'); ?></li>
                                <li><?php _e('redukcję kosztów poprzez wydłużenie okresu ich bezpiecznej eksploatacji, wzrost bezpieczeństwa pracy,', 'sag'); ?></li>
                                <li><?php _e('usprawnienie serwisu,', 'sag'); ?></li>
                                <li><?php _e('kontrolowanie stanu płaskich lin stalowo-gumowych w szybie kopalnianym za pomocą głowicy magnetycznej.', 'sag'); ?></li>
                            </ul>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 39,'page')); ?>"><span class="btn btn--transparent-green"><?php _e('Oferta', 'sag'); ?></span></a>
                        </article>
                        <div class="offer__image offer__image--liny">
                            <p class="image__description"><?php _e('Liny stalowo-gumowe', 'sag'); ?></p>
                        </div>
                        <ul class="offer__submenu">
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 117,'page')); ?>"><li class="offer__submenu__item"><?php _e('Informacje techniczne', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 120,'page')); ?>"><li class="offer__submenu__item"><?php _e('Realizacje', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 122,'page')); ?>"><li class="offer__submenu__item"><?php _e('Pobierz', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 113,'page')); ?>"><li class="offer__submenu__item noborder"><?php _e('Kontakt', 'sag'); ?></li></a>
                        </ul>
                    </div>
                </li>

                <li class="cd-tabs-content-element" data-content="2">
                    <div class="description__container">
                        <article class="description">
                            <h2 class="header--underline"><?php _e('Krążniki', 'sag'); ?></h2>
                            <p class="description__text"><?php _e('Krążniki podtrzymujące taśmę przenośnika taśmowego stanowią najliczniejszą grupę elementów, a&nbsp;od ich sprawnego działania zależą między innymi: prawidłowy bieg taśmy i&nbsp;jej trwałość oraz całkowite opory ruchu taśmy przenośnika. Z&nbsp;tego względu bardzo dużego znaczenia nabiera jakość wykonania i&nbsp;trwałość krążników w czasie ich eksploatacji.', 'sag'); ?></p>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 2,'page')); ?>"><span class="btn btn--transparent-green"><?php _e('Oferta', 'sag'); ?></span></a>
                        </article>
                        <div class="offer__image offer__image--krazniki">
                            <p class="image__description"><?php _e('Krążniki', 'sag'); ?></p>
                        </div>
                        <ul class="offer__submenu">
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 419,'page')); ?>"><li class="offer__submenu__item"><?php _e('Rodzaje', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 421,'page')); ?>"><li class="offer__submenu__item"><?php _e('Badania', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 423,'page')); ?>"><li class="offer__submenu__item"><?php _e('Do pobrania', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 745,'page')); ?>"><li class="offer__submenu__item"><?php _e('Realizacje', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 878,'page')); ?>"><li class="offer__submenu__item noborder"><?php _e('Kontakt', 'sag'); ?></li></a>
                        </ul>
                    </div>
                </li>

                <li class="cd-tabs-content-element" data-content="3">
                    <div class="description__container">
                        <article class="description">
                            <h2 class="header--underline"><?php _e('Konstrukcje stalowe', 'sag'); ?></h2>
                            <p class="description__text"><?php _e('Firma SAG jest producentem szerokiej gamy konstrukcji stalowych z&nbsp;przeznaczeniem dla budownictwa przemysłowego, hutnictwa, energetyki, przemysłu transportowego, cementowni oraz górnictwa. Wykonujemy wszelkiego rodzaju konstrukcje stalowe o&nbsp;wadze jednostkowej do 12,5t. Dysponujemy profesjonalną kadrą nadzoru spawalniczego oraz pracownikami produkcyjnymi posiadającymi wysokie kwalifikacje spawalnicze. Produkcja konstrukcji odbywa się w&nbsp;oparciu o&nbsp;normy EN 1090, PN EN ISO 3834-2, EN 15085.', 'sag'); ?></p>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 56,'page')); ?>"><span class="btn btn--transparent-green"><?php _e('Oferta', 'sag'); ?></span></a>
                        </article>
                        <div class="offer__image offer__image--konstrukcje">
                            <p class="image__description"><?php _e('Konstrukcje stalowe', 'sag'); ?></p>
                        </div>
                        <ul class="offer__submenu">
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 56,'page')); ?>"><li class="offer__submenu__item"><?php _e('Konstrukcje budowlane i dla energetyki', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 885,'page')); ?>"><li class="offer__submenu__item"><?php _e('Konstrukcje kolejowe', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 395,'page')); ?>"><li class="offer__submenu__item"><?php _e('Konstrukcje do przenośników taśmowych', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 392,'page')); ?>"><li class="offer__submenu__item"><?php _e('Konstrukcje szybowe', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 399,'page')); ?>"><li class="offer__submenu__item"><?php _e('Realizacje', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 972,'page')); ?>"><li class="offer__submenu__item"><?php _e('Do pobrania', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 970,'page')); ?>"><li class="offer__submenu__item noborder"><?php _e('Kontakt', 'sag'); ?></li></a>
                        </ul>
                    </div>
                </li>

                <li class="cd-tabs-content-element" data-content="4">
                    <div class="description__container">
                        <article class="description">
                            <h2 class="header--underline"><?php _e('Usługi', 'sag'); ?></h2>
                            <p class="description__text"><?php _e('Firma SAG przy użyciu swojego parku maszynowego świadczy usługi w&nbsp;zakresie: cięcia i&nbsp;gięcia blach i&nbsp;profili, obróbki skrawaniem, spawania ręcznego i&nbsp;zrobotyzowanego, remontów hydrauliki sterowniczej i&nbsp;zawieszeń szybowych oraz badań nieniszczących NDT.', 'sag'); ?></p>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 179,'page')); ?>"><span class="btn btn--transparent-green"><?php _e('Oferta', 'sag'); ?></span></a>
                        </article>
                        <div class="offer__image offer__image--uslugi">
                            <p class="image__description"><?php _e('Usługi', 'sag'); ?></p>
                        </div>
                        <ul class="offer__submenu">
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 191,'page')); ?>"><li class="offer__submenu__item"><?php _e('Cięcie i gięcie blach i profili', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 204,'page')); ?>"><li class="offer__submenu__item"><?php _e('Obróbka skrawaniem', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 207,'page')); ?>"><li class="offer__submenu__item"><?php _e('Spawanie ręczne i zrobotyzowane', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 275,'page')); ?>"><li class="offer__submenu__item"><?php _e('Remonty hydrauliki sterowniczej i zawieszeń szybowych', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 280,'page')); ?>"><li class="offer__submenu__item"><?php _e('Badania nieniszczące NDT', 'sag'); ?></li></a>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 1010,'page')); ?>"><li class="offer__submenu__item noborder"><?php _e('Kontakt', 'sag'); ?></li></a>
                        </ul>
                    </div>
                </li>

                <li class="cd-tabs-content-element" data-content="5">
                    <div class="description__container">
                        <article class="description">
                            <h2 class="header--underline"><?php _e('Handel', 'sag'); ?></h2>
                            <p class="description__text"><?php _e('Firma SAG oprócz działalności produkcyjnej prowadzi działalność w&nbsp;zakresie sprzedaży węgla, lin stalowych i&nbsp;stali.', 'sag'); ?></p>
                            <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 15,'page')); ?>"><span class="btn btn--transparent-green"><?php _e('Oferta', 'sag'); ?></span></a>
                        </article>
                        <div class="offer__image offer__image--handel">
                            <p class="image__description"><?php _e('Handel', 'sag'); ?></p>
                        </div>
                        <ul class="offer__submenu">
                           <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 37, 'page' ) ); ?>"><li class="offer__submenu__item"><?php _e('Sprzedaż węgla', 'sag'); ?></li></a>
                           <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 45, 'page' ) ); ?>"><li class="offer__submenu__item"><?php _e('Sprzedaż lin stalowych', 'sag'); ?></li></a>
                           <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 52, 'page' ) ); ?>"><li class="offer__submenu__item"><?php _e('Sprzedaż stali', 'sag'); ?></li></a>
                           <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 1019, 'page' ) ); ?>"><li class="offer__submenu__item noborder"><?php _e('Kontakt', 'sag'); ?></li></a>
                        </ul>
                    </div>
                </li>

            </ul> <!-- cd-tabs-content -->
        </div> <!-- cd-tabs -->

</div>
</section>
<nav class="navbar navbar-default navbar-fixed-top container-fluid menu" data-spy="affix" data-offset-top="1" id="main-navbar">
    <div class="container navbar__container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" id="main-navbar__button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar__collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>

            <div class="navbar__logo-container">
                <a class="navbar__logo-container__link" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="navbar__logo-container__image" src="<?php bloginfo('template_url'); ?>/images/header/sag-logo.png" alt="SAG"></a>
            </div>
            <div class="navbar__tel-container">
                <svg class="navbar__tel-container__icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" viewBox="0 0 20 19">
                    <defs>
                        <style>
                            .cls-1 {
                                fill: #009848;
                                filter: url(#gradient-overlay-1);
                                fill-rule: evenodd;
                            }
                        </style>
                        <filter id="gradient-overlay-1" filterUnits="userSpaceOnUse">
                            <feImage x="0" y="0" preserveAspectRatio="none" xlink:href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMjAiIGhlaWdodD0iMTkiPjxsaW5lYXJHcmFkaWVudCBpZD0iZ3JhZCIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHkxPSI5LjUiIHgyPSIyMCIgeTI9IjkuNSI+CiAgPHN0b3Agb2Zmc2V0PSIwLjA1IiBzdG9wLWNvbG9yPSIjMDA5ODQ4Ii8+CiAgPHN0b3Agb2Zmc2V0PSIwLjk1IiBzdG9wLWNvbG9yPSIjMGNhYzU4Ii8+CjwvbGluZWFyR3JhZGllbnQ+CjxyZWN0IHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiLz48L3N2Zz4="/>
                            <feComposite operator="in" in2="SourceGraphic"/>
                            <feBlend in2="SourceGraphic" result="gradientFill"/>
                        </filter>
                    </defs>
                    <path d="M19.549,15.034 L16.461,12.092 C15.846,11.509 14.827,11.527 14.191,12.133 L12.635,13.615 C12.537,13.563 12.436,13.509 12.328,13.452 C11.346,12.933 10.001,12.223 8.587,10.874 C7.168,9.523 6.421,8.240 5.875,7.303 C5.817,7.204 5.762,7.108 5.708,7.017 L6.752,6.024 L7.265,5.535 C7.902,4.927 7.920,3.957 7.306,3.372 L4.218,0.430 C3.605,-0.155 2.586,-0.137 1.949,0.471 L1.078,1.305 L1.102,1.327 C0.810,1.682 0.566,2.091 0.385,2.532 C0.218,2.952 0.113,3.352 0.066,3.754 C-0.342,6.974 1.203,9.917 5.395,13.910 C11.189,19.430 15.858,19.013 16.060,18.992 C16.499,18.942 16.919,18.843 17.347,18.684 C17.806,18.513 18.235,18.282 18.607,18.004 L18.626,18.020 L19.507,17.198 C20.144,16.590 20.162,15.621 19.549,15.034 Z" class="cls-1"/>
                </svg>
                <a class="navbar__tel-container__number" href="tel:+48322557260">+48 32 255 72 60</a>
            </div>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-navbar__collapse">
            <div class="menu-container">
                <?php /* Primary navigation */
                wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'depth' => 2,
                        'container' => false,
                        'menu_class' => 'nav navbar-nav',
                        //Process nav menu using our custom nav walker
                        'walker' => new wp_bootstrap_navwalker())
                );
                ?>
            </div>
        </div><!-- /.navbar-collapse -->

    </div><!-- /.container-fluid -->
</nav>
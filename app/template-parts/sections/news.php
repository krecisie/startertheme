<section class="section--full news">
    <div class="news__section-header section-header">
        <h1 class="section-header__text section-header__text--big"><?php _e('Aktualności', 'sag'); ?></h1>
        <h1 class="section-header__text section-header__text--small"><?php _e('Aktualności', 'sag'); ?></h1>
    </div>

    <div class="container-fluid news__container">
        <div class="row">
            <div class="col-xs-12">
                <svg class="news__nav-btn news__nav-btn--left" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="29" height="69" viewBox="0 0 29 69">
                    <a role="button">
                        <defs><style>.cls-2 { fill: #06a350; fill-rule: evenodd; }</style></defs><path d="M3.522 43.025c-4.713-4.714-4.713-12.356 0-17.07L29 .477v68.026L3.522 43.025zM19.197 27.41c.173-.177.173-.463 0-.64-.174-.175-.455-.175-.628 0l-7.743 7.867c-.12.123-.126.29-.078.445-.05.155-.043.322.077.444l7.742 7.866c.172.176.453.176.627 0 .173-.176.173-.46 0-.638l-7.552-7.672 7.552-7.673z" class="cls-2"/>
                    </a>
                </svg>


                <svg class="news__nav-btn news__nav-btn--right" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="29" height="69" viewBox="0 0 29 69">
                    <a role="button">
                        <defs><style>.cls-3 { fill: #06a350; fill-rule: evenodd; }</style></defs><path d="M25.478 43.045L0 68.523V.497l25.478 25.478c4.713 4.714 4.713 12.356 0 17.07zm-7.305-9.57l-7.742-7.867c-.172-.176-.453-.176-.627 0-.173.176-.173.46 0 .638l7.552 7.672-7.552 7.673c-.173.177-.173.463 0 .64.174.175.455.175.628 0l7.743-7.867c.12-.123.126-.29.078-.445.05-.155.043-.322-.077-.444z" class="cls-3"/>
                    </a>
                </svg>
                <div class="news-carousel loop owl-carousel">
                    <?php $the_query = new WP_Query( array( 'cat' => 5 ) ); ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="news-carousel__item">
                                <div class="news-carousel__image" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                                <div class="news-carousel__text-wrapper">
                                    <h2 class="news-carousel__header"><?php the_title(); ?></h2>
<!--                                    <p class="news-carousel__text news-carousel__text--green">--><?php //echo get_the_date(); ?><!--</p>-->
                                    <div class="news-carousel__text"><?php the_excerpt(); ?></div>
                                </div>
                                <div class="news-carousel__btn btn"><a href="<?php the_permalink(); ?>"><?php _e('Czytaj więcej', 'sag'); ?></a></div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
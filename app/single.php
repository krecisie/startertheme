<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sag
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <section class="section--full page-offer page-wpis">

                <div class="container">
                    <div class="col-xs-12 col-md-7">
                        <div class="col-xs-12 col-md-12">

                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                            ?>

                            <div class="col-xs-12 col-md-12">
                                <div class="head_banner" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-9 col-md-9">
                                <div class="top__white">
                                    <p><strong><?php the_title();?></strong></p>
<!--                                    <p>Data dodania: <span>--><?php //the_date('d/m/y'); ?><!--</span></p>-->
                                </div>
                            </div>
                        </div>



                        <div class="col-xs-12 col-md-12 text_float">
                            <?php the_content(); ?>




<!--                            <div class="col-xs-12 col-md-12">-->
<!--                                <div class="flex__container">-->
<!---->
<!--                                    <ul class="flex-container wrap">-->
<!--                                        <li class="flex-item flex-item__1">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item flex-item__2">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                        <li class="flex-item">-->
<!--                                            <img src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->



<!--                            <div class="col-xs-12 col-md-12 flex__container_line">-->
<!--                                <p class="flex__container_signature"> --><?php //_e('Kliknij na zdjęcie by powiększyć', 'sag'); ?><!--</p>-->
<!--                            </div>-->
                            <hr>
                            <?php
                            endwhile; else: ?>
                                <p><?php _e('Brak wpisów.', 'sag'); ?></p>
                            <?php endif; ?>



                            <!-- Left-aligned-arrow -->
                            <?php
                            $prev_post = get_previous_post();
                            if (!empty( $prev_post )): ?>
                                <div class="col-xs-6 col-md-6 arrow_object">
                                    <a class="pull-left" href="<?php echo get_permalink( $prev_post->ID ); ?>">
                                        <img class="pull-left arrow_object__img__left"  <?php next_image_link(); ?> src="<?php bloginfo('template_url'); ?>/images/posts/arrow_l.png">
                                        <span class="arrow_heading arrow_body__left"><?php posts_nav_link(); ?><?php _e('Poprzedni wpis', 'sag'); ?></span>
                                    </a>
                                </div>
                            <?php endif ?>
                            <!-- Right-aligned-arrow -->
                            <?php
                            $next_post = get_next_post();
                            if (!empty( $next_post )): ?>
                                <div class="col-xs-6 col-md-6 arrow_object">
                                    <a class="pull-right" href="<?php echo get_permalink( $next_post->ID ); ?>">
                                        <img class="pull-left arrow_object__img__right" src="<?php bloginfo('template_url'); ?>/images/posts/arrow_l.png">
                                        <span class="arrow_heading arrow_body__right" <?php previous_post_link( $in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ); ?>><?php _e('Następny wpis', 'sag'); ?></span>
                                    </a>
                                </div>
                            <?php endif ?>



<!--                            <!-- Left-aligned -->
<!--                            --><?php
//                            $prev_post = get_previous_post();
//                            if (!empty( $prev_post )): ?>
<!--                                <div class="col-xs-6 col-md-6 media_object media_object__left">-->
<!--                                    <div class="media">-->
<!--                                        <a class="pull-left" href="--><?php //echo $prev_post->guid ?><!--">-->
<!--                                            <img class="pull-left media_object__img" src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </a>-->
<!---->
<!---->
<!--                                        <div class="media-body media-body__left">-->
<!--                                            <a class="media-heading__link" href="--><?php //echo $prev_post->guid ?><!--">-->
<!--                                                <h4 class="media-heading">Poprzedni tytuł</h4></a>-->
<!--                                            <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            --><?php //endif ?>
<!--                            <!-- Right-aligned -->
<!--                            --><?php
//                            $next_post = get_next_post();
//                            if (!empty( $next_post )): ?>
<!--                                <div class="col-xs-6 col-md-6 media_object">-->
<!--                                    <div class="media">-->
<!--                                        <a class="pull-right" href="--><?php //echo get_permalink( $next_post->ID ); ?><!--">-->
<!--                                            <img class="pull-left media_object__img" src="--><?php //bloginfo('template_url'); ?><!--/images/posts/next_thread.png">-->
<!--                                        </a>-->
<!--                                        <div class="media-body media-body__right">-->
<!--                                            <a class="media-heading__link" href="--><?php //echo get_permalink( $next_post->ID ); ?><!--"><h4 class="media-heading"> Następny tytuł</h4></a>-->
<!--                                            <p>nie mowcie ze to takie proste--><?php //the_excerpt(); ?><!--</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            --><?php //endif ?>

                            <?php
                            $prevPost = get_previous_post(true);
                            $nextPost = get_next_post(true);
                            ?>

                            <?php $prevPost = get_previous_post(true);
                            if($prevPost) {
                                $args = array(
                                    'posts_per_page' => 1,
                                    'include' => $prevPost->ID
                                );
                                $prevPost = get_posts($args);
                                foreach ($prevPost as $post) {
                                    setup_postdata($post);
                                    ?>
                            <div class="col-xs-6 col-md-6 media_object">
                                    <div class="post-previous">
                                        <!-- ADD THE THUMBNIAL AND LINK IT TO THE POST -->
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail', array('class' => 'media_object__img_left')); ?></a>
                                        <!-- ALSO WITH THE TITLE -->
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <!-- SHOW THE CUTOM EXCERPT LENGTH -->
                                        <p><?php $content = get_the_content(); echo wp_trim_words( $content , '28' ); ?></p>
                                        <!-- FINALLY SHOW AN ACTUAL NAVIGATIONAL PROMPT -->
                                    </div>
                            </div>
                                    <?php
                                    wp_reset_postdata();
                                } //end foreach
                            } // end if

                            $nextPost = get_next_post(true);
                            if($nextPost) {
                                $args = array(
                                    'posts_per_page' => 1,
                                    'include' => $nextPost->ID
                                );
                                $nextPost = get_posts($args);
                                foreach ($nextPost as $post) {
                                    setup_postdata($post);
                                    ?>
                            <div class="col-xs-6 col-md-6 media_object">
                            <div class="post-next">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail', array('class' => 'media_object__img_right')); ?></a>
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <p><?php $content = get_the_content(); echo wp_trim_words( $content , '28' ); ?></p>

                                    </div>
                            </div>
                                    <?php
                                    wp_reset_postdata();
                                } //end foreach
                            } // end if
                            ?>

                        </div>
                    </div>

                    <div class="col-xs-12 col-md-4 col-md-push-1">
                        <?php
                        get_sidebar();
                        ?>
                    </div>
                </div>

            </section>  <?php
            get_footer();?>
                   <?php
//                    while ( have_posts() ) : the_post();
//
//                        get_template_part( 'template-parts/content', get_post_format() );
//
//                        the_post_navigation();
//
////			// If comments are open or we have at least one comment, load up the comment template.
////			if ( comments_open() || get_comments_number() ) :
////				comments_template();
////			endif;
//
//                    endwhile; // End of the loop.
//                    ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();?>
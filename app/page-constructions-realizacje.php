<?php
/**
Template name: Constructions-realizacje
 */

get_header(); ?>

    <section class="section--full page-offer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-7 page-offer__content">
                    <div class="page-styles-default">
                        <div class="head_banner head_banner--subpage" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                            <div class="subpage__title-field">
                                <h1><?php the_title();?></h1>
                            </div>
                        </div>
                        <div class="page-offer__gallery">
                            <ul class="menu__round menu__round--full">
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 56,'page')); ?>"><li class="round__item round__item--first"><?php _e('Konstrukcje budowlane i dla energetyki', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 885,'page')); ?>"><li class="round__item"><?php _e('Konstrukcje kolejowe', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 395,'page')); ?>"><li class="round__item"><?php _e('Konstrukcje do przenośników taśmowych', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 392,'page')); ?>"><li class="round__item"><?php _e('Konstrukcje szybowe', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 399,'page')); ?>"><li class="round__item"><?php _e('Realizacje', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 972,'page')); ?>"><li class="round__item"><?php _e('Do pobrania', 'sag'); ?></li></a>
                                <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 970,'page')); ?>"><li class="round__item round__item--last"><?php _e('Kontakt', 'sag'); ?></li></a>
                            </ul>
                        </div>
                        <?php
                        while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                            <?php
                        endwhile;
                        wp_reset_query();
                        ?>
<!--                        <div class="page-offer__gallery">-->
<!--                            <ul class="menu__round menu__round--full">-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 56,'page')); ?><!--"><li class="round__item round__item--first">--><?php //_e('Konstrukcje budowlane i dla energetyki', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 885,'page')); ?><!--"><li class="round__item">--><?php //_e('Konstrukcje kolejowe', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 395,'page')); ?><!--"><li class="round__item">--><?php //_e('Konstrukcje do przenośników taśmowych', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 392,'page')); ?><!--"><li class="round__item">--><?php //_e('Konstrukcje szybowe', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 399,'page')); ?><!--"><li class="round__item">--><?php //_e('Realizacje', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 972,'page')); ?><!--"><li class="round__item">--><?php //_e('Do pobrania', 'sag'); ?><!--</li></a>-->
<!--                                <a href="--><?php //echo get_permalink( apply_filters( 'wpml_object_id', 970,'page')); ?><!--"><li class="round__item round__item--last">--><?php //_e('Kontakt', 'sag'); ?><!--</li></a>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </div>

                    <div class="portfolio--flex">
                        <div class="portfolio--flex-content">
                            <ul class="slider-content nav nav-tabs owl-carousel">
                                <li data-hash="slajd-1">
                                    <h2><?php _e( 'Człon środkowy konstrukcji tramwaju', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-SAM_2784.jpg" target="_blank"><button class="btn btn--transparent-green"> <?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-2">
                                    <h2><?php _e( 'Człon środkowy konstrukcji tramwaju', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-WP_20160826_013.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-3">
                                    <h2><?php _e( 'Konstrukcja podestu podwieszanego pomostu wiszącego', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-IMG_2909.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-4">
                                    <h2><?php _e( 'Konstrukcja podestu podwieszanego pomostu wiszącego', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/06/podest-2.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-5">
                                    <h2><?php _e( 'Elementy trasy przenośnika taśmowego', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-WP_20161025_009.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-6">
                                    <h2><?php _e( 'Elementy trasy przenośnika taśmowego', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/07/elementy-trasy.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-7">
                                    <h2><?php _e( 'Konstrukcja przesiewacza', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-IMG_1488.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-8">
                                    <h2><?php _e( 'Bęben do nawijania liny', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-20170609_115950.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>
                                <li data-hash="slajd-9">
                                    <h2><?php _e( 'Rama wózka do tramwaju', 'sag'); ?></h2>

                                    <a href="<?php echo site_url(); ?>/wp-content/uploads/2017/06/wozek.jpg" target="_blank"><button class="btn btn--transparent-green"><?php _e( 'Powiększ', 'sag'); ?></button></a>
                                </li>

                            </ul>
                        </div>
                        <div class="portfolio--flex-slider">
                            <ul class="slider-images slider-images-krazniki nav nav-tabs owl-carousel" role="presentation">
                                <li role="presentation" data-hash="slajd-1">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/07/uslugi-realizacje-SAM_2784-small.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Człon środkowy konstrukcji tramwaju', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-2">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/07/uslugi-realizacje-WP_20160826_013-small.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Człon środkowy konstrukcji tramwaju', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-3">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-IMG_2909.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Konstrukcja podestu podwieszanego pomostu wiszącego', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-4">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/06/podest-2.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Konstrukcja podestu podwieszanego pomostu wiszącego', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-5">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/07/uslugi-realizacje-WP_20161025_009-small.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Elementy trasy przenośnika taśmowego', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-6">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/07/elementy-trasy-small.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Elementy trasy przenośnika taśmowego', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-7">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/05/uslugi-realizacje-IMG_1488.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Konstrukcja przesiewacza', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-8">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/07/uslugi-realizacje-20170609_115950-small.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Bęben do nawijania liny', 'sag'); ?></span>
                                </li>
                                <li role="presentation" data-hash="slajd-9">
                                    <img src=<?php echo site_url(); ?>/wp-content/uploads/2017/07/wozek-small2.jpg alt="Konstrukcje realizacje" width="965px" height="535px"/>
                                    <span><?php _e( 'Rama wózka do tramwaju', 'sag'); ?></span>
                                </li>
                            </ul>
                            <div class="slider-menu--nav" role="navigation">
                                <button class="arrow left">
                                    <svg viewBox="0 0 50 80" xml:space="preserve">
<polyline fill="none" stroke="#1a1a1a" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
45.63,75.8 0.375,38.087 45.63,0.375 "/>
</svg>
                                </button>
                                <button class="arrow right">
                                    <svg viewBox="0 0 50 80" xml:space="preserve">
<polyline fill="none" stroke="#1a1a1a" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
0.375,0.375 45.63,38.087 0.375,75.8 "/>
</svg>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-md-4 col-md-push-1">
                    <?php
                    get_sidebar();
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();?>